package com.voteadmin.repository;

import com.voteadmin.model.CheckCPF;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "check", url = "https://user-info.herokuapp.com/users")
public interface CheckCPFRepository {

    @RequestMapping(method = RequestMethod.GET, value = "/{cpf}", produces = "application/json")
    CheckCPF checkCPF(@PathVariable("cpf") String cpf);

}
