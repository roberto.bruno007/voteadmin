package com.voteadmin.repository;

import com.voteadmin.model.Meeting;
import com.voteadmin.model.Session;
import com.voteadmin.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VoteRepository extends JpaRepository<Vote,Long> {

    @Query("select v.cpf from Vote v where v.meeting = :meeting AND v.cpf = :value")
    String existsByCPF(@Param("value")String value, @Param("meeting") Meeting meeting);

}
