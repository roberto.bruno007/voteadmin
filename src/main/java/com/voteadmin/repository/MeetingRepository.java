package com.voteadmin.repository;

import com.voteadmin.model.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MeetingRepository extends JpaRepository<Meeting,Long> {

    @Query("select m from Meeting m where m.cpfOwner = :cpf")
    Optional<Meeting> findBycpf(@Param("cpf") String cpf);

    Boolean existsByName(String name);

}
