package com.voteadmin.repository;

import com.voteadmin.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SessionRepository extends JpaRepository<Session,Long> {

    @Modifying
    @Query("update Session s set s.totalVotes = totalVotes + 1 where s.id = :id")
    void updateTotalVotesSession(@Param("id")Long id);

}
