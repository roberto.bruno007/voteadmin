package com.voteadmin.mapper;

import com.voteadmin.dto.MeetingResponse;
import com.voteadmin.model.Meeting;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDateTime;

@Mapper(componentModel="spring")
public interface MeetingMapper {

    MeetingResponse toMeetingResponse(Meeting meeting);

    @Mapping(source = "session.dateCreate", target = "session.dateCreate", qualifiedByName = "toString")
    @Mapping(source = "session.dateUpdate", target = "session.dateUpdate", qualifiedByName = "toString")
    @Mapping(source = "session.dateExpiration", target = "session.dateExpiration", qualifiedByName = "toString")
    @Mapping(source = "dateCreate", target = "dateCreate", qualifiedByName = "toString")
    @Mapping(source = "dateUpdate", target = "dateUpdate", qualifiedByName = "toString")
    com.voteadmin.avro.Meeting toAvro(Meeting meeting);

    @Named("toString")
    public static String toString(LocalDateTime inch) {
        return inch.toString();
    }
}
