package com.voteadmin.mapper;

import com.voteadmin.dto.SessionResponse;
import com.voteadmin.model.Session;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface SessionMapper {

    SessionResponse toSessionResponse(Session session);

}
