package com.voteadmin.mapper;

import com.voteadmin.dto.VoteResponse;
import com.voteadmin.model.Vote;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface VoteMapper {

    VoteResponse toVoteResponse(Vote vote);

}
