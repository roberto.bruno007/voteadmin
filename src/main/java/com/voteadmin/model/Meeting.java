package com.voteadmin.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Representa uma Pauta
 */
@Getter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "voteadmin",name = "meeting")
public class Meeting {

    @Id
    @SequenceGenerator(name = "voteadmin.meeting_id_seq", sequenceName = "voteadmin.meeting_id_seq", allocationSize = 1, initialValue = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "voteadmin.meeting_id_seq")
    private Long id;
    private String name;
    @Column(name = "cpf_owner")
    private String cpfOwner;
    @ManyToOne
    @JoinColumn(name = "session_meeting_id")
    private Session session;
    private boolean approve;
    private LocalDateTime dateCreate;
    private LocalDateTime dateUpdate;

    public void addSession(Session sessionResult) {
        this.session = sessionResult;
    }

    public void approve() {
        this.approve = true;
    }
}
