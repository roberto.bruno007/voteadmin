package com.voteadmin.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Representa uma Sessao
 */
@Getter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "voteadmin",name = "session_meeting")
public class Session {

    @Id
    @SequenceGenerator(name = "voteadmin.session_meeting_id_seq", sequenceName = "voteadmin.session_meeting_id_seq", allocationSize = 1, initialValue = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "voteadmin.session_meeting_id_seq")
    private Long id;
    private int totalVotes;
    private LocalDateTime dateCreate;
    private LocalDateTime dateUpdate;
    private LocalDateTime dateExpiration;

}
