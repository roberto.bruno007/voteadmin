package com.voteadmin.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Representa um Voto
 */
@Getter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = "voteadmin",name = "vote")
public class Vote {
    @Id
    @SequenceGenerator(name = "voteadmin.vote_id_seq", sequenceName = "voteadmin.vote_id_seq", allocationSize = 1, initialValue = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "voteadmin.vote_id_seq")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "meeting_id")
    private Meeting meeting;
    private String cpf;
    @Column(name = "value_vote")
    private String value;
    private LocalDateTime dateCreate;

}
