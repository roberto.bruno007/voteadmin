package com.voteadmin.resource.v1;

import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.dto.CreateVoteRequest;
import com.voteadmin.dto.SessionResponse;
import com.voteadmin.dto.VoteResponse;
import com.voteadmin.mapper.VoteMapper;
import com.voteadmin.model.Vote;
import com.voteadmin.service.VoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;

@Api(tags = {"Api para manipular operacoes referente a Votacao"})
@SwaggerDefinition(tags = {@Tag(name = "Vote")})
@RestController
@RequestMapping(CONFIGPATH.BASE_API_URL_V1)
@RequiredArgsConstructor
public class VoteResource {

    private final VoteService voteService;
    private final VoteMapper mapper;


    @ApiOperation(value="Registra um Voto", notes="Registra um voto de acordo com o ID da Pauta, a resposta da criacao sera o voto registrado")
    @PostMapping("/vote")
    public ResponseEntity<VoteResponse> createVote(@Valid @RequestBody CreateVoteRequest createVoteRequest, HttpServletResponse response){
        Vote vote = this.voteService.save(createVoteRequest);
        VoteResponse voteResponse = mapper.toVoteResponse(vote);
        return new ResponseEntity<VoteResponse>(voteResponse, HttpStatus.CREATED);
    }

}
