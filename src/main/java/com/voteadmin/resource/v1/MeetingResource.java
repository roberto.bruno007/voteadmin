package com.voteadmin.resource.v1;

import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.dto.CreateMeetingRequest;
import com.voteadmin.dto.MeetingResponse;
import com.voteadmin.mapper.MeetingMapper;
import com.voteadmin.model.Meeting;
import com.voteadmin.service.MeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.hibernate.validator.constraints.Range;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.net.URI;

@Api(tags = {"Api para manipular operacoes referente a Pauta"})
@SwaggerDefinition(tags = {@Tag(name = "Meeting")})
@RestController
@RequestMapping(CONFIGPATH.BASE_API_URL_V1)
public class MeetingResource {

    private final MeetingService meetingService;
    private final MeetingMapper mapper;

    public MeetingResource(MeetingService meetingService, MeetingMapper mapper) {
        this.meetingService = meetingService;
        this.mapper = mapper;
    }

    @ApiOperation(value="Cria uma Pauta", notes="Cria uma Pauta")
    @PostMapping("/meeting")
    public ResponseEntity<Void> createMeeting(@Valid @RequestBody CreateMeetingRequest meetingRequest, HttpServletResponse response){
        Meeting model = this.meetingService.save(meetingRequest);
        return ResponseEntity.created(URI.create(CONFIGPATH.BASE_API_URL_V1+"/meeting/"+model.getId())).build();
    }

    @ApiOperation(value="Recupera um Meeting", notes="Recupera um Meeting de acordo com o ID informado")
    @GetMapping("/meeting/{id}")
    public ResponseEntity<MeetingResponse> getMeeting(@Valid @Range(min = 1) @PathVariable(name="id",required=true) Long id){
        MeetingResponse meetingResponse = mapper.toMeetingResponse(this.meetingService.findById(id));
        return ResponseEntity.ok(meetingResponse);
    }

    @ApiOperation(value="Finaliza uma Pauta", notes="Finaliza uma Pauta")
    @PutMapping("/meeting/approve/{id}")
    public ResponseEntity<MeetingResponse> finishMeeting(@Valid @Range(min = 1) @PathVariable(name="id",required=true) Long id, HttpServletResponse response){
        Meeting meeting = this.meetingService.approveMeeting(id);
        MeetingResponse meetingResponse = mapper.toMeetingResponse(meeting);
        return ResponseEntity.ok(meetingResponse);
    }

}
