package com.voteadmin.resource.v1;

import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.dto.CreateSessionRequest;
import com.voteadmin.dto.SessionResponse;
import com.voteadmin.mapper.SessionMapper;
import com.voteadmin.model.Session;
import com.voteadmin.service.SessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.hibernate.validator.constraints.Range;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.net.URI;

@Api(tags = {"Api para manipular operacoes referente a Sessao"})
@SwaggerDefinition(tags = {@Tag(name = "Session")})
@RestController
@RequestMapping(CONFIGPATH.BASE_API_URL_V1)
public class SessionResource {

    private final SessionService sessionService;
    private final SessionMapper mapper;

    public SessionResource(SessionService sessionService, SessionMapper mapper) {
        this.sessionService = sessionService;
        this.mapper = mapper;
    }

    @ApiOperation(value="Abre uma Sessao", notes="Abra uma Sessao de acordo com o id de uma Pauta, caso dateExpiration seja nullo, a sessao sera de apenas um minuto")
    @PostMapping("/session")
    public ResponseEntity<Void> createSession(@Valid @RequestBody CreateSessionRequest createSessionRequest, HttpServletResponse response){
        Session session = this.sessionService.save(createSessionRequest);
        return ResponseEntity.created(URI.create(CONFIGPATH.BASE_API_URL_V1+"/session/"+session.getId())).build();
    }

    @ApiOperation(value="Recupera uma Sessao", notes="Recupera uma Sessao de acordo com o ID informado")
    @GetMapping("/session/{id}")
    public ResponseEntity<SessionResponse> getSession(@Valid @Range(min = 1) @PathVariable(name="id",required=true) Long id){
        SessionResponse sessionResponse = mapper.toSessionResponse(this.sessionService.findById(id));
        return ResponseEntity.ok(sessionResponse);
    }

}
