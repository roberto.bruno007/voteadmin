package com.voteadmin.producer;

import com.google.gson.Gson;
import com.voteadmin.mapper.MeetingMapper;
import com.voteadmin.model.Meeting;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
@Service
@RequiredArgsConstructor
public class MeetingProducer {

    @Value("${topic.name.producer}")
    private String topicName;

    private final MeetingMapper mapper;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void send(com.voteadmin.model.Meeting meeting){
        com.voteadmin.avro.Meeting meetingAvro = mapper.toAvro(meeting);
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(createProducerRecord(meetingAvro));

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("Sent message= "+toJson(meetingAvro)+" with offset=["+result.getRecordMetadata().offset()+"]");
            }
            @Override
            public void onFailure(Throwable ex) {
                log.info("Unable to send message="+toJson(meetingAvro),ex.getMessage());
            }
        });
        kafkaTemplate.flush();
    }

    private ProducerRecord<String, String> createProducerRecord(com.voteadmin.avro.Meeting meeting) {
        return new ProducerRecord<String, String>(topicName, toJson(meeting));
    }

    private String toJson(com.voteadmin.avro.Meeting meeting){
        return new Gson().toJson(meeting);
    }

}
