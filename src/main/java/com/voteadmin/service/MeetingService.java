package com.voteadmin.service;

import com.voteadmin.dto.CreateMeetingRequest;
import com.voteadmin.exceptionHandler.CreateMessageExeception;
import com.voteadmin.mapper.MeetingMapper;
import com.voteadmin.model.Meeting;
import com.voteadmin.producer.MeetingProducer;
import com.voteadmin.repository.MeetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MeetingService {

    private final MeetingRepository meetingRepository;
    private final MeetingMapper mapper;
    private final MeetingProducer meetingProducer;

    @Transactional
    public Meeting save(CreateMeetingRequest createMeetingRequest){
        if(this.meetingRepository.existsByName(createMeetingRequest.getName())){
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.meeting.name","");
        }

        Meeting meeting = Meeting.builder()
                .name(createMeetingRequest.getName())
                .cpfOwner(createMeetingRequest.getCpfOwner())
                .dateCreate(LocalDateTime.now())
                .dateUpdate(LocalDateTime.now())
                .build();
        return this.meetingRepository.save(meeting);
    }

    @Transactional
    public Meeting save(Meeting meeting){
        return this.meetingRepository.save(meeting);
    }

    public Meeting findById(Long id){
        return this.meetingRepository.findById(id).orElseThrow(() -> CreateMessageExeception.createObjectMessageNotFoundExeception("messagem.notfound",""+id));
    }

    @Transactional
    public Meeting approve(Long id){
        Meeting meeting = this.findById(id);
        if(meeting.getSession()!=null) {
            if(LocalDateTime.now().isAfter(meeting.getSession().getDateExpiration())){
                meeting.approve();
            }else{
                throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.not.approve","");
            }
        }else{
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.meeting.notexist.session","");
        }

        return this.meetingRepository.save(meeting);
    }

    public Meeting approveMeeting(Long id){
        Meeting meeting = approve(id);
        send(meeting);
        return meeting;
    }

    public void send(Meeting meeting){
        this.meetingProducer.send(meeting);
    }

}
