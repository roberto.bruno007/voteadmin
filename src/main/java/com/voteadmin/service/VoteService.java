package com.voteadmin.service;

import com.voteadmin.dto.CreateVoteRequest;
import com.voteadmin.exceptionHandler.CreateMessageExeception;
import com.voteadmin.model.CheckCPF;
import com.voteadmin.model.Meeting;
import com.voteadmin.model.Session;
import com.voteadmin.model.Vote;
import com.voteadmin.repository.VoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class VoteService {

    private final VoteRepository voteRepository;
    private final CheckCPFService checkCPFService;
    private final MeetingService meetingService;
    private final SessionService sessionService;

    @Transactional
    public Vote save(CreateVoteRequest createVoteRequest){

        Meeting meeting = meetingService.findById(createVoteRequest.getIdMeeting());
        if (meeting.getSession() == null) {
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.meeting.session","");
        }

        checkCPF(createVoteRequest.getCpf());
        checkCPFInData(createVoteRequest.getCpf(),meeting);

        Session session = meeting.getSession();
        checkDateExpirate(session);

        Vote vote = Vote.builder()
                .cpf(createVoteRequest.getCpf())
                .value(createVoteRequest.getValue().toString())
                .dateCreate(LocalDateTime.now())
                .meeting(meeting)
                .build();

        Vote voteResult = this.voteRepository.save(vote);
        this.sessionService.updateTotalVotesSession(session);
        return voteResult;
    }

    /**
     * Verifica se a data em questao e menor q a data de expiracao
     * @param session
     */
    private void checkDateExpirate(Session session) {
        if(LocalDateTime.now().withNano(0).isAfter(session.getDateExpiration())){
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.date.expiration","");
        }
    }

    private void checkCPFInData(String cpf,Meeting meeting) {
        if(this.voteRepository.existsByCPF(cpf,meeting)!=null){
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.cpf.vote","");
        }
    }

    private void checkCPF(String cpf) {
        CheckCPF checkCPF = this.checkCPFService.verify(cpf);
        if(checkCPF.getStatus().equals("UNABLE_TO_VOTE")){
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.cpf.notvote","");
        }

    }

}
