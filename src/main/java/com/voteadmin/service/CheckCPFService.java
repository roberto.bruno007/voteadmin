package com.voteadmin.service;

import com.voteadmin.exceptionHandler.CreateMessageExeception;
import com.voteadmin.model.CheckCPF;
import com.voteadmin.repository.CheckCPFRepository;
import org.springframework.stereotype.Service;

@Service
public class CheckCPFService {

    private final CheckCPFRepository checkCPFRepository;

    public CheckCPFService(CheckCPFRepository checkCPFRepository) {
        this.checkCPFRepository = checkCPFRepository;
    }

    public CheckCPF verify(String cpf){
        try {
            return this.checkCPFRepository.checkCPF(cpf);
        }catch (Exception ex){
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.notcomunication.url","");
        }
    }

}
