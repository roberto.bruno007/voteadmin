package com.voteadmin.service;

import com.voteadmin.dto.CreateSessionRequest;
import com.voteadmin.exceptionHandler.CreateMessageExeception;
import com.voteadmin.model.Meeting;
import com.voteadmin.model.Session;
import com.voteadmin.repository.SessionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class SessionService {

    private final SessionRepository sessionRepository;
    private final MeetingService meetingService;

    @Transactional
    public Session save(CreateSessionRequest sessionRequest){
        checkDataExpiration(sessionRequest);

        Meeting meeting = this.meetingService.findById(sessionRequest.getIdMeeting());
        if(meeting.getSession()!=null){
            throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.meeting.exist.session","");
        }
        Session session = Session.builder()
                .dateCreate(LocalDateTime.now())
                .dateUpdate(LocalDateTime.now())
                .dateExpiration(creteDateExpiration(sessionRequest))
                .build();

        Session sessionResult = this.sessionRepository.save(session);
        meeting.addSession(sessionResult);
        this.meetingService.save(meeting);
        return sessionResult;
    }

    /**
     * Verifa se a data de expiracao existe e se ela é uma data posterior
     * a data desse exato momento.
     * @param sessionRequest
     */
    private void checkDataExpiration(CreateSessionRequest sessionRequest) {
        if(sessionRequest.getDateExpiration() != null){
            LocalDateTime dateCurrent = sessionRequest.getDateExpiration();
            LocalDateTime dateNow = LocalDateTime.now().withNano(0);
            if(dateCurrent.withNano(0).isBefore(dateNow)){
                throw CreateMessageExeception.createObjectMessageBadRequestExeception("message.date.after","");
            }
        }
    }

    /**
     * Retorna uma data de expiracao para uma @Session. Caso a data em questao seja nula
     * sera adicionando um minuto para o tempo de expiracao
     * @param sessionRequest
     * @return
     */
    private LocalDateTime creteDateExpiration(CreateSessionRequest sessionRequest) {
        LocalDateTime dateCurrent = sessionRequest.getDateExpiration();
        LocalDateTime dateNow = LocalDateTime.now().withNano(0);
        return dateCurrent==null
                ?dateNow.plusMinutes(1)
                :dateCurrent.withNano(0);
    }

    public Session findById(Long id){
        return this.sessionRepository.findById(id).orElseThrow(() -> CreateMessageExeception.createObjectMessageNotFoundExeception("messagem.notfound",""+id));
    }

    @Transactional
    public void updateTotalVotesSession(Session session) {
        this.sessionRepository.updateTotalVotesSession(session.getId());
    }

}
