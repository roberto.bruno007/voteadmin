package com.voteadmin.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class MeetingResponse {

    private final Long id;
    private final String name;
    private final String cpfOwner;
    private final boolean approve;
    private final SessionResponse session;
    private final LocalDateTime dateCreate;
    private final LocalDateTime dateUpdate;

}
