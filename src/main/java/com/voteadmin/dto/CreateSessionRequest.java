package com.voteadmin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Builder
public class CreateSessionRequest {

    @ApiModelProperty(example = "1",required = true,notes = "Representa o id de uma Pauta que ja foi criada")
    @NotNull
    @Range(min = 1)
    private final Long idMeeting;
    @ApiModelProperty(example = "2021-11-10T12:00:00",required = false,notes = "Representa uma data de expiracao, caso ela seja nula, a expiracao sera de um minuto")
    private final LocalDateTime dateExpiration;

}
