package com.voteadmin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.br.CPF;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Builder
public class CreateMeetingRequest {

    @ApiModelProperty(example = "Ata de reuniao",required = true)
    @NotNull
    private final String name;


    @Pattern(regexp="(^$|[0-9]{11})",message = "Somente numeros com ate 11 digitos")
    @NotNull
    @CPF
    @ApiModelProperty(example = "15899310090",required = true)
    private final String cpfOwner;

}
