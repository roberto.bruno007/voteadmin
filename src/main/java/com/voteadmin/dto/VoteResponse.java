package com.voteadmin.dto;

import lombok.Builder;
import lombok.Getter;
import java.time.LocalDateTime;

@Getter
@Builder
public class VoteResponse {

    private Long id;
    private MeetingResponse meeting;
    private String cpf;
    private String value;
    private LocalDateTime dateCreate;

}
