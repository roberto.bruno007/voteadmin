package com.voteadmin.dto;

import com.voteadmin.enums.VOTEVALUE;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Builder
public class CreateVoteRequest {

    @ApiModelProperty(example = "1",required = true,notes = "Representa o id da Pauta que deseja votar")
    @NotNull
    private final Long idMeeting;


    @Pattern(regexp="(^$|[0-9]{11})",message = "Somente numeros com ate 11 digitos")
    @NotNull
    @CPF
    @ApiModelProperty(example = "15899310090",required = true)
    private final String cpf;

    @ApiModelProperty(example = "SIM",required = true)
    @NotNull
    private final VOTEVALUE value;
}
