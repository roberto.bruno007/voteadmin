package com.voteadmin.dto;

import lombok.Builder;
import lombok.Getter;
import java.time.LocalDateTime;

@Getter
@Builder
public class SessionResponse {
    private final Long id;
    private final int totalVotes;
    private final String status;
    private final LocalDateTime dateCreate;
    private final LocalDateTime dateUpdate;
    private final LocalDateTime dateExpiration;

}
