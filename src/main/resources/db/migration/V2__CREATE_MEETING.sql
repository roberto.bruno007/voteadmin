CREATE TABLE voteadmin.meeting (
	id bigint NOT NULL,
	name varchar(100) NOT NULL,
	approve boolean NOT NULL,
	cpf_owner varchar(50) NOT NULL,
	session_meeting_id bigint,
	date_create timestamp NOT NULL,
	date_update timestamp NOT NULL,
	CONSTRAINT meeting_name_unique UNIQUE (name),
	CONSTRAINT meeting_pkey PRIMARY KEY (id),
	FOREIGN KEY (session_meeting_id) REFERENCES voteadmin.session_meeting (id)
);

CREATE SEQUENCE voteadmin.meeting_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER TABLE voteadmin.meeting ALTER COLUMN id SET DEFAULT NEXTVAL('voteadmin.meeting_id_seq'::regclass);


