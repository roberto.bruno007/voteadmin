CREATE TABLE voteadmin.vote (
	id bigint NOT NULL,
	meeting_id int NOT NULL,
	cpf varchar(50) NOT NULL,
	value_vote varchar(5) NOT NULL,
	date_create timestamp NOT NULL,
	CONSTRAINT vote_pkey PRIMARY KEY (id),
	CONSTRAINT vote_cpf_unique UNIQUE (cpf,id),
	FOREIGN KEY (meeting_id) REFERENCES voteadmin.meeting (id)
);

CREATE SEQUENCE voteadmin.vote_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER TABLE voteadmin.vote ALTER COLUMN id SET DEFAULT NEXTVAL('voteadmin.vote_id_seq'::regclass);

