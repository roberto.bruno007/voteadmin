CREATE TABLE voteadmin.session_meeting (
	id bigint NOT NULL,
	total_votes bigint,
	date_create timestamp NOT NULL,
	date_update timestamp NOT NULL,
	date_expiration timestamp NOT NULL,
	CONSTRAINT session_meetingg_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE voteadmin.session_meeting_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER TABLE voteadmin.session_meeting ALTER COLUMN id SET DEFAULT NEXTVAL('voteadmin.session_meeting_id_seq'::regclass);


