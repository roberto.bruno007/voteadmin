package com.voteadmin.resource.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.config.PostgresqlContainerCustom;
import com.voteadmin.databuilder.DataBuilder;
import com.voteadmin.dto.CreateSessionRequest;
import com.voteadmin.repository.CheckCPFRepository;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.containers.PostgreSQLContainer;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SessionResourceTest {
    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = PostgresqlContainerCustom.getInstance();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    CheckCPFRepository checkCPFRepository;

    @Test
    public void testCreateSessionWithoutDateExpiration() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao do condominio","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        CreateSessionRequest sessionRequest = CreateSessionRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .build();
        MvcResult mockMvcResult = mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/session")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(sessionRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        String pathSession = mockMvcResult.getResponse().getHeader("Location").toString();
        mockMvc.perform(get(pathSession)
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateSessionWithDateExpiration() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre estacionamento 2","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        CreateSessionRequest sessionRequest = CreateSessionRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .dateExpiration(LocalDateTime.now().plusMinutes(3))
                .build();
        MvcResult mockMvcResult = mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/session")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(sessionRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        String pathSession = mockMvcResult.getResponse().getHeader("Location").toString();
        mockMvc.perform(get(pathSession)
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateSessionWithDateExpirationBeforeDateNow() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre estacionamento","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        CreateSessionRequest sessionRequest = CreateSessionRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .dateExpiration(LocalDateTime.of(2021,10,13,9,30))
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/session")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(sessionRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("A data de expiracao precisa ser maior que a data de hoje."));

    }

}
