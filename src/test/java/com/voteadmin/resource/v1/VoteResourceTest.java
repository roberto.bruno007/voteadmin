package com.voteadmin.resource.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.config.PostgresqlContainerCustom;
import com.voteadmin.databuilder.DataBuilder;
import com.voteadmin.dto.CreateVoteRequest;
import com.voteadmin.enums.VOTEVALUE;
import com.voteadmin.model.CheckCPF;
import com.voteadmin.repository.CheckCPFRepository;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class VoteResourceTest {
    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = PostgresqlContainerCustom.getInstance();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    CheckCPFRepository checkCPFRepository;

    @Test
    public void testCreateVote() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre Biblioteca","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), null,mockMvc,objectMapper);

        Mockito.when(checkCPFRepository.checkCPF("49335626015"))
                .thenReturn(CheckCPF.builder()
                        .status("ABLE_TO_VOTE")
                        .build());

        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("49335626015")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.cpf").value("49335626015"))
                .andExpect(jsonPath("$.value").value("SIM"));
    }

    @Test
    public void testCreateVoteForSameCPF() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre pedagio","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), null,mockMvc,objectMapper);

        Mockito.when(checkCPFRepository.checkCPF("15899310090"))
                .thenReturn(CheckCPF.builder()
                            .status("ABLE_TO_VOTE")
                            .build());


        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("15899310090")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        CreateVoteRequest secondVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("15899310090")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(secondVoteRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("O CPF ja fez uma Votacao"));

    }

    @Test
    public void testCreateVoteWithCPF_UNABLE_TO_VOTE() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre Bicicletar","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), null,mockMvc,objectMapper);
        Mockito.when(checkCPFRepository.checkCPF("49335626015"))
                .thenReturn(CheckCPF.builder()
                        .status("UNABLE_TO_VOTE")
                        .build());

        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("49335626015")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("De acordo com a URL o CPF esta invalido para votar."));
    }

    @Test
    public void testCreateVoteInSessionExpirade() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre esgoto","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), LocalDateTime.now().plusSeconds(3),mockMvc,objectMapper);

        TimeUnit.SECONDS.sleep(4);

        Mockito.when(checkCPFRepository.checkCPF("49335626015"))
                .thenReturn(CheckCPF.builder()
                        .status("ABLE_TO_VOTE")
                        .build());

        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("49335626015")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("A sessao ja esta expirada par votacao"));
    }

    @Test
    public void testCreateVoteWithCPFInvalid() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre a inluminacao","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), null,mockMvc,objectMapper);

        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("12345678912")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.[0].messageErro").value("CPF invalido"));
    }

    @Test
    public void testCreateVoteWithoutSession() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Ata de reuniao sobre a acessibilidade","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(meetingResponse.get("id").getAsLong())
                .cpf("49335626015")
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("Para realizar um voto e preciso ter uma sessao criada"));
    }

}
