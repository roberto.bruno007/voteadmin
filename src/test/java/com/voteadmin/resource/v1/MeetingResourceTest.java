package com.voteadmin.resource.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.databuilder.DataBuilder;
import com.voteadmin.config.PostgresqlContainerCustom;
import com.voteadmin.dto.CreateMeetingRequest;
import com.voteadmin.repository.CheckCPFRepository;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.containers.PostgreSQLContainer;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })
public class MeetingResourceTest {

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = PostgresqlContainerCustom.getInstance();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    CheckCPFRepository checkCPFRepository;

    @Test
    public void testCreateMeeting() throws Exception {

        CreateMeetingRequest meetingRequest = CreateMeetingRequest.builder()
                .name("Ata de reuniao")
                .cpfOwner("30266208070")
                .build();

        MvcResult mvcResult =   mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/meeting")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(meetingRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        String path = mvcResult.getResponse().getHeader("Location").toString();
        mockMvc.perform(get(path)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.approve").value("false"))
                .andExpect(jsonPath("$.cpfOwner").value("30266208070"));

    }

    @Test
    public void testCreateMeetingWithNameExist() throws Exception {
        DataBuilder.createMeeting("Pauta test","60276028058",mockMvc,objectMapper);
        CreateMeetingRequest meetingRequest = CreateMeetingRequest.builder()
                .name("Pauta test")
                .cpfOwner("30266208070")
                .build();

        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/meeting")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(meetingRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("Ja existe uma pauta com esse nome"));

    }

    @Test
    public void testCreateMeetingAndApproval() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Pauta 1","60276028058",mockMvc,objectMapper),
                        JsonObject.class);
        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), LocalDateTime.now().plusSeconds(3),mockMvc,objectMapper);
        DataBuilder.createVote(meetingResponse.get("id").getAsLong(),"30266208070","nao",
                mockMvc,objectMapper,checkCPFRepository);
        DataBuilder.createVote(meetingResponse.get("id").getAsLong(),"49335626015","nao",
                mockMvc,objectMapper,checkCPFRepository);
        DataBuilder.createVote(meetingResponse.get("id").getAsLong(),"15899310090","sim",
                mockMvc,objectMapper,checkCPFRepository);

        TimeUnit.SECONDS.sleep(3);

        mockMvc.perform(put(CONFIGPATH.BASE_API_URL_V1+"/meeting/approve/"+meetingResponse.get("id").getAsLong()+"")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.approve").value("true"))
                .andExpect(jsonPath("$.session.totalVotes").value(3));;
    }

    @Test
    public void testCreateMeetingAndApprovalBeforeDateExpiration() throws Exception {
        JsonObject meetingResponse = new Gson()
                .fromJson(DataBuilder.createMeeting("Pauta 21","60276028058",mockMvc,objectMapper),
                        JsonObject.class);
        DataBuilder.createSession(meetingResponse.get("id").getAsLong(), null,mockMvc,objectMapper);
        DataBuilder.createVote(meetingResponse.get("id").getAsLong(),"30266208070","nao",
                mockMvc,objectMapper,checkCPFRepository);
        DataBuilder.createVote(meetingResponse.get("id").getAsLong(),"49335626015","nao",
                mockMvc,objectMapper,checkCPFRepository);
        DataBuilder.createVote(meetingResponse.get("id").getAsLong(),"15899310090","sim",
                mockMvc,objectMapper,checkCPFRepository);

        mockMvc.perform(put(CONFIGPATH.BASE_API_URL_V1+"/meeting/approve/"+meetingResponse.get("id").getAsLong()+"")
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("A Pauta so pode ser aprovado depois que a sessao estiver expirada"));
    }

    @Test
    public void testCreateMeetingAndApprovalWithoutSession() throws Exception {
        JsonObject meetingResponse = new Gson().fromJson(
                DataBuilder.createMeeting("Pauta 201","60276028058",mockMvc,objectMapper),
                JsonObject.class);

        mockMvc.perform(put(CONFIGPATH.BASE_API_URL_V1+"/meeting/approve/"+meetingResponse.get("id").getAsLong()+"")
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.messageErro").value("Essa Pauta nao pode ser aprovada, pois ele nao tem uma Sessao."));
    }

}
