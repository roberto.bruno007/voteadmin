package com.voteadmin.databuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.voteadmin.config.CONFIGPATH;
import com.voteadmin.dto.CreateMeetingRequest;
import com.voteadmin.dto.CreateSessionRequest;
import com.voteadmin.dto.CreateVoteRequest;
import com.voteadmin.enums.VOTEVALUE;
import com.voteadmin.model.CheckCPF;
import com.voteadmin.repository.CheckCPFRepository;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DataBuilder {
    public static String createMeeting(String name, String cpf, MockMvc mockMvc, ObjectMapper objectMapper) throws Exception {
        CreateMeetingRequest meetingRequest = CreateMeetingRequest.builder()
                .name(name)
                .cpfOwner(cpf)
                .build();

        MvcResult mvcResult =  mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/meeting")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(meetingRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        String path = mvcResult.getResponse().getHeader("Location").toString();
        return mockMvc.perform(get(path)
                        .contentType("application/json"))
                .andReturn().getResponse().getContentAsString();
    }

    public static String createSession(Long idMeetin, LocalDateTime dateExpiration, MockMvc mockMvc,ObjectMapper objectMapper) throws Exception {

        CreateSessionRequest sessionRequest = CreateSessionRequest.builder()
                .idMeeting(idMeetin)
                .dateExpiration(dateExpiration)
                .build();
        MvcResult mockMvcResult = mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/session")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(sessionRequest)))
                .andExpect(status().isCreated())
                .andReturn();

        String pathSession = mockMvcResult.getResponse().getHeader("Location").toString();
        return mockMvc.perform(get(pathSession)
                        .contentType("application/json"))
                .andReturn().getResponse().getContentAsString();

    }

    public static void createVote(Long idMeeting, String cpf, String value, MockMvc mockMvc, ObjectMapper objectMapper, CheckCPFRepository checkCPFRepository) throws Exception {

        Mockito.when(checkCPFRepository.checkCPF(cpf))
                .thenReturn(CheckCPF.builder()
                        .status("ABLE_TO_VOTE")
                        .build());

        CreateVoteRequest createVoteRequest = CreateVoteRequest.builder()
                .idMeeting(idMeeting)
                .cpf(cpf)
                .value(VOTEVALUE.SIM)
                .build();
        mockMvc.perform(post(CONFIGPATH.BASE_API_URL_V1+"/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(createVoteRequest)));
    }

}
