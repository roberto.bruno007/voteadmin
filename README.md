# voteadmin

![Semantic description of image](/img/caso_de_uso.png "Image Title")

O projeto esta utilizando as seguintes tecnológias :

* SpringBoot
* Postgres
* Jacoco
* TestContainer(https://www.testcontainers.org)
* Swagger
* flyway
* Kafka
* docker-compose
* MapStruct

Para inicar o projeto basta seguir os seguintes passo, você precisar ter o docker instaldo na sua maquina para poder rodar os testes :

1. Pare o Postgres local da sua maquina, caso vc tenha alguma rodando .
2. Pare o Kafka local da sua maquina, caso vc tenha alguma rodando
3. Abra o terminal e entre na raiz do projeto. `cd voteadmin/`
4. Rode o comando para gerar o .jar. `mvn clean package`
5. Posteriormente baster rodar o comando do docker-compose. `docker-compose up --build -d`
6. Para realizar os testes manuais va para a pagina do swagger. `http://localhost:8080/swagger-ui.html#`
7. Para vizualizar o Dashboard do Kafka `http://localhost:3030/kafka-topics-ui`

O swagger esta com todas as funcionalidades para realizar os testes iniciais:
1. Criar uma Pauta  ***POST /voteadmin/v1/meeting***
<!-- blank line -->
   `{
   "cpfOwner": "15899310090",
   "name": "Ata de reuniao"
   }`

2. Abrir uma Sessão ***POST /voteadmin/v1/session***
<!-- blank line -->
   `{
   "idMeeting": 1
   }`

3. Realizar um Voto ***POST /voteadmin/v1/vote***
<!-- blank line -->
   `{
   "cpf": "15899310090",
   "idMeeting": 1,
   "value": "SIM"
   }`
   
4. Aprovar uma Pauta ***PUT /voteadmin/v1/meeting/approve/{id}***
<!-- blank line -->
5. Sigua os examples que estão no swagger para facilitar os testes


Para abrir o projeto em uma IDE, sera necessário gerar as classes que estão sendo utilizadas no projeto 
1. Rode o comando `mvn generate-sources` pra gerar as classes Avro .
2. Rode o comando `mvn clean package -Dmaven.test.skip` para gerar as classes do MapStruct .



Estou utilizando o Jacoco para verificar a cobertuda dos testes, rode o comando dentro da 
pasta do projeto `mvn clean package` , para vizualizar o dashboard. Logo em seguida abra o 
index.html(voteadmin/target/site/index.html) em um browser de sua preferencia, o minimo de 
aceitação esta de 60%,porém os testes atuais estão em 73%:
<!-- blank line -->
![Semantic description of image](/img/dashboard_jacoco.png "Image Title")
